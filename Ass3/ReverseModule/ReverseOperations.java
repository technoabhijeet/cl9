package ReverseModule;


/**
* ReverseModule/ReverseOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ReverseModule.idl
* Wednesday, 8 April, 2020 8:53:01 AM IST
*/

public interface ReverseOperations 
{
  String reverse_string (String str);
} // interface ReverseOperations
